# SFDX and Friends

Light weight (alpine based) docker image with Salesforce sfdx, node LTS and misc utilities. Suitable for simple automation tasks

Available as `sorenkrabbe/sfdx-and-friends` through [docker hub](https://hub.docker.com/r/sorenkrabbe/sfdx-and-friends)

## Builds
* 4/9/2020
* 10/6/2021
